#include "Vector3.h"

/// <summary>
/// Default Constructor,
/// initializes member variables to 0.0
/// </summary>
Vector3::Vector3() :
	m_x(0.0),
	m_y(0.0),
	m_z(0.0)
{
}

/// <summary>
/// Overloaded Constructor,
/// initializes member variables to the passed parameters
/// </summary>
/// <param name="x"> defines x coordinate </param>
/// <param name="y"> defines y coordinate </param>
/// <param name="z"> defines z coordinate </param>
Vector3::Vector3(double x, double y, double z) :
	m_x(x),
	m_y(y),
	m_z(z)
{
}

/// <summary>
/// Gets the x component of the vector
/// </summary>
/// <returns> double as the x component </returns>
double Vector3::getX() const
{
	return m_x;
}

/// <summary>
/// Gets the y component
/// </summary>
/// <returns> double as they component </returns>
double Vector3::getY() const
{
	return m_y;
}

/// <summary>
/// Gets the z component
/// </summary>
/// <returns> double as thez component </returns>
double Vector3::getZ() const
{
	return m_z;
}

/// <summary>
/// Calculates length of current Vector3
/// </summary>
/// <returns> Returns the length </returns>
double Vector3::length() const
{
	return sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
}

/// <summary>
/// Calculates the length squared of current Vector3
/// </summary>
/// <returns> Returns the length squared </returns>
double Vector3::lengthSquare() const
{
	return (m_x * m_x + m_y * m_y + m_z * m_z);
}

/// <summary>
/// Reduces the length of the vector to 1.0
/// keeping the direction the same
/// </summary>
void Vector3::normalise()
{
	double magnitude = length();
	if (magnitude > 0)
	{
		m_x /= magnitude;
		m_y /= magnitude;
		m_z /= magnitude;
	}

}

/// <summary>
/// Gets returns a string from the vector
/// following this format (0.0, 0.0, 0.0)
/// </summary>
/// <returns> returns a formatted string </returns>
std::string Vector3::toString() const
{
	return "(" + std::to_string(m_x) + ", " + std::to_string(m_y) + ", " + std::to_string(m_z) + ")";
}

/// <summary>
/// Overloaded operator+
/// adds 2 vectors
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns the sum of 2 vectors </returns>
Vector3 operator+(const Vector3 & lhs, const Vector3 & rhs)
{
	return Vector3(lhs.m_x + rhs.m_x, lhs.m_y + rhs.m_y, lhs.m_z + rhs.m_z);
}

/// <summary>
/// Overloaded operator-
/// substracts 2 vectors
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> return the difference of 2 vectors </returns>
Vector3 operator-(const Vector3 & lhs, const Vector3 & rhs)
{
	return Vector3(lhs.m_x - rhs.m_x, lhs.m_y - rhs.m_y, lhs.m_z - rhs.m_z);
}

/// <summary>
/// Overloaded operator-
/// negates the vector
/// </summary>
/// <param name="v"> vector to be negated </param>
/// <returns> returns the vector with negative components </returns>
Vector3 operator-(const Vector3 & v)
{
	return Vector3(-v.m_x, -v.m_y, -v.m_z);
}

/// <summary>
/// Overloaded operator*
/// Gets the Scalar product between 2 vectors
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns the scalar product of 2 vectors </returns>
double operator*(const Vector3 & lhs, const Vector3 & rhs)
{
	return (lhs.m_x * rhs.m_x + lhs.m_y * rhs.m_y + lhs.m_z * rhs.m_z);
}

/// <summary>
/// Overloaded operator*
/// Gets the product of a scalar with a vector
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns vector product of a scalar by a vector </returns>
Vector3 operator*(const Vector3 & lhs, const double & rhs)
{
	return Vector3(lhs.m_x * rhs, lhs.m_y * rhs, lhs.m_z * rhs);
}

/// <summary>
/// Overloaded operator*
/// Gets the product of a scalar with a vector
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns vector product of a scalar by a vector </returns>
Vector3 operator*(const Vector3 & lhs, const float & rhs)
{
	return Vector3(lhs.m_x * rhs, lhs.m_y * rhs, lhs.m_z * rhs);
}

/// <summary>
/// Overloaded operator*
/// Gets the product of a scalar with a vector
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns scalar product of a scalar by a vector </returns>
Vector3 operator*(const Vector3 & lhs, const int & rhs)
{
	return Vector3(lhs.m_x * rhs, lhs.m_y * rhs, lhs.m_z * rhs);
}

/// <summary>
/// Overloaded operator^
/// cross product of 2 vectors
/// </summary>
/// <param name="lhs"> left hand side of operator </param>
/// <param name="rhs"> right hand side of operator </param>
/// <returns> returns the cross product of 2 vectors </returns>
Vector3 operator^(const Vector3 & lhs, const Vector3 & rhs)
{
	return Vector3(
		lhs.m_y * rhs.m_z - lhs.m_z * rhs.m_y,
		lhs.m_z * rhs.m_x - rhs.m_x * rhs.m_z,
		lhs.m_x * rhs.m_y - lhs.m_y * rhs.m_x);
}
